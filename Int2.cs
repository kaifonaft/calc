﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace курсовая_2011
{
    class Int2
    {
        public int[] num;
        public int len;
        public const int lenmas=40;
        private const string digits="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";        
        public int radix;//base
//        int precision;
        public Int2 clone()
        {
            Int2 t = new Int2();
            for (int i = 0; i < lenmas; i++)
                t.num[i] = num[i];
            t.radix = radix;
            t.len = len;
            return t;
        }
        public Int2(string s, int r=10){
            if (r < 2 || r > 36) throw new Exception("incorrect radix");
            if (s.Length > lenmas)throw new Exception("too long number");
            s=s.ToUpper();
            bool neg = false;
            if (s[0] == '-'){
                neg = true;
                s = s.Substring(1);
            }
            num = new int[lenmas];
            radix = r;
            for (int i = 0; i <s.Length; i++)
            {
                int t = -1;
                for (int j = 0; j < r; j++)
                    if (s[s.Length-i-1] == digits[j])
                    {
                        t = j;
                        break;
                    }
                if (t == -1) throw new Exception(s +" is not a number or radix is wrong");
                else num[lenmas - i - 1] = t;
            }
            if (neg) uminus();
        }
        public Int2(int a = 0, int r = 10)
        {
            num = new int[lenmas];
            radix = r;
            assignment(a, r);
/*            radix = r;
            len = 0;
            int i=lenmas-1;
            int b = a;
            if (a < 0) b = -b;
            while (i >= 0 && b != 0) {
                num[i] = b % radix;
                b /= radix;
                len++;
                i--;
            }
            if (b != 0) throw new Exception("too long number");
            if (a < 0) uminus();
 */
        }
        void invert() {
            for (int i = 0; i < lenmas; i++) num[i] = radix - num[i]-1;
        }
        public bool is_neg() {
            if (radix % 2 == 0)
            {
                if (num[0] >= radix / 2) return true;
            }
            else
            {
                int i=0;
                while (i < lenmas && num[i]==radix/2)i++;
                if (num[i] > radix / 2)return true;
                else return false;
            }
            return false;
        }
        public void uminus(){
            bool is_neg_b=is_neg();
  
            int p = radix - 1, q = 0, add=1;
// если отриц то вычитаем еденицу и инвертируем
// иначе инвертируем и добавляем еденицу
            if (is_neg_b)
            {
                p = 0;
                q = radix - 1;
                add = -1;
            } else {
                invert();
            }
            int j = lenmas - 1;
            while (j >= 0 && num[j] == p)
            {
                num[j] = q;
                j--;
            }
            num[j]+=add;
            if (is_neg_b)invert();
        }
        override public string ToString() {
            string s = "";
            Int2 t = this.clone();
            if (is_neg())
            {
                s = "-";
                t.uminus();
            }
            int i = 0;
            while (i < lenmas && t.num[i] == 0) i++;
            if (i == lenmas) s = "0";
            while (i < lenmas)
            {
                s += digits[t.num[i]];
                i++;
            }
            return s;
        }
        public int ToNum()
        {
            int s = 0,pow=1;
            Int2 t = this.clone();
            if (is_neg()) t.uminus();
            for (int i = lenmas - 1; i >= 0; i--)
            {
                s += t.num[i] * pow;
                pow *= t.radix;
            }
            if (is_neg()){
//                uminus();
                s = -s;
            }
            return s;
        }
        public void assignment(int a,int r)
        {
            radix = r;
//            num = new int[lenmas];
//            radix = r;
            len = 0;
            int i=lenmas-1;
            for (int j = 0; j < lenmas; j++)
                num[j] = 0;
            int t = a;
            if (a < 0) t = -t;
            while (i >= 0 && t != 0)
            {
                num[i] = t % radix;
                t /= radix;
                len++;
                i--;
            }
            if (a < 0) uminus();
            if (t != 0) throw new Exception("too long number");
        }
       public void add(Int2 v) {
            Int2 t = new Int2(v.ToNum(), radix);
            int inc = 0;
            for (int i = lenmas-1; i >= 0; i--) { 
                int a=num[i]+t.num[i]+inc;
                if(a>=radix)inc=1;
                else inc = 0;
                num[i] = a % radix;
            }
        }
        public void sub(Int2 v) {
            Int2 t = new Int2(v.ToNum(), radix);
            t.uminus();
            this.add(t);
        }
        void shiftleft(int n) {
            int[] m = new int[lenmas];
            for (int i = 0; i < lenmas; i++){
                m[i] = num[i];
                num[i]=0;
            }
            for (int i = 0; i < lenmas; i++)
                if (i - n >= 0 && i - n < lenmas) num[i - n] = m[i];
        }
        public void mul(Int2 v) {
            int sign = 1;
            Int2 p=this.clone(), q=new Int2(v.ToNum(),radix), sum=new Int2(0,radix), w=new Int2(0,radix);
            if (is_neg()) {
                p.uminus();
                sign *= -1;
            }
            if (v.is_neg()) {
                q.uminus();
                sign *= -1;
            }
            for(int i=0; i<lenmas; i++)
                for (int j = 0; j < lenmas; j++) {
                    w.assignment(p.num[i]*q.num[j],radix);
                    w.shiftleft(2*lenmas-i-j-2);
                    sum.add(w);
                }
            if (sign < 0) sum.uminus();
            for (int i = 0; i < lenmas; i++)
                num[i] = sum.num[i];
        }
    }
}
