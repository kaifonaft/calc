﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace курсовая_2011
{
    class Double2
    {
        int sign;
        int power;
        int[] fr;
        int radix;//base
        public const int lenmas=32;
        private static int calc_mode=CalcMode.real;

        public const string digits = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        public static void setCalcMode(int m) {
            if (m == CalcMode.int16 || m == CalcMode.int32 || m == CalcMode.real) calc_mode = m;
            else throw new Exception("incorrect calc mode");
        }
        public static int getCalcMode() { return calc_mode; }
        public int[] getFr() {
            int[] m = new int[lenmas];
            for (int i = 0; i < lenmas; i++)
                m[i] = fr[i];
            return m;
        }
        public int getPow(){
            return power;
        }
        public int getSign() {
            return sign;
        }
        private int f(char c) {
            if (c == '0') return symbol.zero;
            if (c == '.' || c==',') return symbol.point;
            for (int i = 0; i < radix; i++)
                if (digits[i] == c) return symbol.digit;
            return symbol.another;
        }
        public int getRadix(){
            return radix;
        }
        public bool isZero() {
            for (int i = 0; i < lenmas; i++)
                if (fr[i] != 0) return false;
            return true;
        }
        public Double2(Int2 a) {
            fr = new int[lenmas];
            if (a.is_neg())
            {
                sign = -1;
                a.uminus();
            } else sign = 1;
            radix=a.radix;
            int j=0;
            while(j<Int2.lenmas && a.num[j]==0)j++;
            power=Int2.lenmas-j;
            int t=j;
            while(j<Int2.lenmas){
                fr[j-t]=a.num[j];
                j++;
            }
            if (isZero()) power = 0;
        }
        public void uminus(){
            sign=-sign;
        }
        public int int_part(){
            if (power <= 0) return 0;
            int p = 1,sum=0;
            for (int i = 0; i < power; i++) {
                sum += p * fr[power - i - 1];
                p *= radix;
            }
            return sum*sign;
        }
        unsafe override public string ToString() {
            string s = "";
            int t=0,g;
            if (calc_mode != CalcMode.real)
            {
                g = int_part();
                string si;
                if (calc_mode == CalcMode.int16){
                    int* p = &g;
                    short* p1 = ((short*)p);
//                    if (g < 0) *p1 = (short)((*p1) | (1<<15));
                    si = (*p1).ToString();
                    g = *p1;
//                   if (g < 0) si = "-"+si;
//                    si = ((short)g).ToString();
                }else{
                    si = g.ToString();
                }
                Double2 w=new Double2(si, 10);
                w.ch_radix(radix,1);
                if (w.power == 0) return "0";
                if (g < 0) s = "-";
                while (t<lenmas && t < w.power){
                    s += digits[w.fr[t]];
                    t++;
                }
                while (t < w.power){
                    s += "0";
                    t++;
                }
                return s;
            }
            if (power > 0)
            {
                while (t<lenmas && t < power){
                    s += digits[fr[t]];
                    t++;
                }
                while (t < power){
                    s += "0";
                    t++;
                }
                s += ".";
            } else {
                s = "0.";
                g=power;
                while (g < 0) {
                    s += "0";
                    g++;
                }
            }
            g = lenmas - 1;
            while(g>=t && fr[g]==0)g--;
            if (g < t) s += "0";
            for(int i=t; i<=g; i++)
                s+=digits[fr[i]];
            if (sign < 0 && s != "0.0") s = "-" + s;
            return s;
        }
        public void ch_radix(int r,int mode=0){
            Double2 int_p, fr_p, mult;
            Int2 a;
            a = new Int2(int_part(), r);
            int_p = new Double2(a);
            a = new Int2(r, radix);
            mult = new Double2(a);
            fr_p = this.clone();
            if(power>0)fr_p.shift_right(-power);
            int lm2=lenmas*2;
            int[] fr2 = new int[lm2];
            int t,newpow;

            for (int i = 0; i < lm2; i++) {
                fr_p.mul(mult);
                fr2[i]=fr_p.sign*fr_p.int_part();
                if(fr_p.power>0)fr_p.shift_right(-fr_p.power);
            }
//            if(power<0)
            if(power>0){
                power=int_p.power;
                t=int_p.power;
            }else{
                int h=0;
                while (h<fr2.Length && fr2[h] == 0) h++;
                for (int i = 0; i < lm2; i++)
                    if (i + h < lm2) fr2[i] = fr2[i + h];
                    else break;
                power = -h;
                t=0;
            }
            for (int i = 0; i < t; i++)
                fr[i] = int_p.fr[i];
            for (int i = t; i < lenmas; i++)
                fr[i] = fr2[i-t];
            radix = r;
            if (isZero()) power = 0;
        }
        public Double2(string s="0", int r=10) {
            if (r < 2 || r > 36) throw new Exception("incorrect radix");
            if (s[0] == '-'){
                sign = -1;
                s = s.Substring(1);
            }else sign = 1;
            int a = s.Length;
            if (s.IndexOf(".") != -1 || s.IndexOf(",") != -1){
                a--;
                if (s.IndexOf("0.") == 0 || s.IndexOf("0,") == 0) a--;
            }
            if (a > lenmas) throw new Exception("too long number");
            radix = r;
            s = s.ToUpper();
            for (int i = 0; i < s.Length; i++)
                if (s[i] == ',') s = s.Substring(0,i)+"."+s.Substring(i+1);
            fr = new int[lenmas];
            for(int i=0; i<lenmas; i++)fr[i]=0;
            power = 0;
            int[][] av = new int[5][];
            av[0] = new int[] { 0,  1, 3, -1 };
            av[1] = new int[] { 1, -1, 2, -1 };
            av[2] = new int[] { 2, -1, 2, -1 };
            av[3] = new int[] { 3,  4, 3, -1 };
            av[4] = new int[] { 4, -1, 4, -1 };
            int state=0;
            int di=-1;
            if (s == "." || s==",") throw new Exception("incorrect number");
            
            int g=0;
            while(g<s.Length && s[g]=='0')g++;
            if (g > 1) s = s.Substring(g-1);
            if (s[0]=='0' && s.Length > 1 && !(s[1] == '.' || s[1] == ',')) s = s.Substring(1);
            
            for (int i = 0; i < s.Length; i++) {
                state = av[state][f(s[i])];
                if (state == -1) throw new Exception("incorrect number");
                if (f(s[i])==symbol.point) di = i;
                if (state == 3) power++;
                if (state == 1 && s[i]=='0') power--;
            }
            if (power < 0){
                s = s.Substring(di - power + 1);
                if (s == "") power = 0;
            } else {
                if (di == -1)
                {
                    di = s.Length;
                    s = s+'.';
                }
                s = s.Substring(di - power);
                di = s.IndexOf(".");
                s = s.Substring(0, di) + s.Substring(di + 1);
            }
            // s -> fraction
            for (int i = 0; i < s.Length; i++)
                for (int j = 0; j < radix; j++)
                    if (s[i] == digits[j])
                    {
                        fr[i] = j;
                        break;
                    }

//            double.Parse(s);

  /*          if (s[0] == '.') {
                j = 1;
                while
            } else { 
            
            }
  */
            if (isZero()) power = 0;
        }
        public void shift_right(int n) {
            power += n;
/*
            for (int i = lenmas - n - 1; i >= 0; i--)
                fraction[i+n]=fraction[i];
            for (int i = 0; i < n; i++)
                fraction[i] = 0;
*/
            int[] m = new int[lenmas];
            for (int i = 0; i < lenmas; i++)
            {
                m[i] = fr[i];
                fr[i] = 0;
            }
            for (int i = 0; i < lenmas; i++)
                if (i + n >= 0 && i + n < lenmas) fr[i + n] = m[i];

        }
        public Double2 clone() {
            Double2 t = new Double2();
            t.sign=sign;
            t.power=power;
            t.fr = new int[lenmas];
            for (int i = 0; i < lenmas; i++)
                t.fr[i] = fr[i];
            t.radix=radix;
            return t;
        }
        public void add(Double2 v)
        {
            int[] fr2 = new int[lenmas+1];
            int inc = 0;
            Double2 p=this.clone(), q = v.clone(),t;
            if (v.radix != radix) q.ch_radix(radix);
//            if (q.radix != p.radix) q.ch_radix(p.radix);
            int d=p.power - q.power;
            if (d < 0)
            {
                p.shift_right(-d);
                power = q.power;
            }
            else
            {
                q.shift_right(d);
                power = p.power;
            }
            for (int i = 0; i < lenmas; i++)
                fr[i] = 0;
            if (q.sign == p.sign) {
                for (int i = lenmas - 1; i >= 0; i--)
                {
                    int a = p.fr[i] + q.fr[i] + inc;
                    if (a >= radix) inc = 1;
                    else inc = 0;
                    fr2[i+1] = a % radix;
                }
                if (inc == 1)
                {
                    fr2[0] = 1;
                    power++;
                }
                for (int i = 0; i < lenmas; i++)
                    fr[i] = fr2[i + 1 - inc];
            } else {
                sign=p.sign;
                for (int i = 0; i < lenmas; i++)
                    if (p.fr[i] < q.fr[i]) {
                        sign = q.sign;
                        t = p;
                        p = q;
                        q = t;
                        break;
                    } else if (p.fr[i] > q.fr[i]) break; 
                for (int i = lenmas - 1; i >= 0; i--)
                {
                    int a = p.fr[i] - q.fr[i] - inc;
                    if (a < 0)
                    {
                        inc = 1;
                        a = radix + a;
                    }
                    else
                    {
                        inc = 0;
                    }
                    fr[i] = a;
                }
                int j=0;
                while (j<lenmas && fr[j] == 0) j++;
                shift_right(-j);
//                for (int i = 0; i < lenmas; i++)
//                    fr[i] = fr2[i + 1 - inc];
            }
//            t.shift_right();
            if (isZero()) power = 0;
        }
        public void sub(Double2 v)
        {
            Double2 t = v.clone();
            t.sign = -t.sign;
            this.add(t);
        }
        public void mul(Double2 v) {
            Double2 p = this.clone(), q = v.clone(), sum = new Double2("0", radix), w = new Double2("0", radix);
            if (v.radix != radix){
                q.ch_radix(radix,1);
            }
            sign = sign * v.sign;
            power = p.power + q.power;
            int[] fr2 = new int[lenmas * 2], fr3 = new int[lenmas * 2];
            int inc=0,k;
            for (int i = 0; i < lenmas; i++)
            {
                for (int j = 0; j < lenmas; j++)
                {
                    int a = p.fr[i] * q.fr[j];
                    //произведение i-ой и j-ой цифры в fr3
                    fr3[i+j]= a / radix;
                    fr3[i+j+1] = a % radix;
                    // fr2 += fr3:
                    k = i + j + 1;
                    inc=0;
                    while (k >= 0) {
                        int t=(fr3[k] + fr2[k]+inc);
                        fr2[k] = t % radix;
                        inc = t / radix;
                        k--;
                    }
                    fr3[i + j] = 0;
                    fr3[i + j + 1] = 0;
                }
            }
            k=0;
            while (k < lenmas && fr2[k] == 0)k++;
            power -= k;
            for (int i = 0; i < lenmas; i++)
                fr[i] = fr2[i + k];
            if (isZero()) power = 0;
/*            if (k == lenmas) { 
                k=0;
                while(k<lenmas && fr[k]==0)k++;
                shift_right(-k);
            }
 */

        }
        /*
            if (r < 2 || r > 36) throw new Exception("incorrect radix");
            if (s.Length > lenmas)throw new Exception("too long number");
            s=s.ToUpper();
            bool neg = false;
            if (s[0] == '-'){
                neg = true;
                s = s.Substring(1);
            }
            num = new int[lenmas];
            radix = r;
            for (int i = 0; i <s.Length; i++)
            {
                int t = -1;
                for (int j = 0; j < r; j++)
                    if (s[s.Length-i-1] == digits[j])
                    {
                        t = j;
                        break;
                    }
                if (t == -1) throw new Exception(s +" is not a number or radix is wrong");
                else num[lenmas - i - 1] = t;
            }
            if (neg) uminus();
         */
    }
}
