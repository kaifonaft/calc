﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace курсовая_2011
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        Int2 v;
        Int2 var1, var2;
        Double2 w1, w2, wc,w3;
        int state_calc=0;
        int current_radix=10;
        int state_oper = 0,last_oper,lm_oper;
        const string float_prompt = "s   exponent    fraction";
        private bool is_rdigit(char c) {
           c=char.ToUpper(c);
            for (int i = 0; i < current_radix; i++)
                if (c == Double2.digits[i]) return true;
            return false;
        }
        unsafe private string bit_float(float f){
            string s1 = "";
            float* p = &f;
            int q = *((int*)p);
            if (q < 0)
            {
                f = -f;
                q = *((int*)p);
                s1 += "1";
            }
            else
            {
                s1 += "0";
            }
            int one = 1<<30;
            s1 += "  ";
            for(int i=1; i<32; i++){
                if (i == 9) s1 += "   ";
                if ((one & q) != 0) s1 += "1";
                else s1 += "0";
                one >>= 1;
            }
            return s1;
        }
        private void c_radix_Click(object sender, EventArgs e)
        {
            operation(Oper.ch_radix);
        }

        private void uminus_Click(object sender, EventArgs e)
        {
            operation(Oper.uminus);
        }
        private void sp_float_uncheck(){
            NAN.Checked = false;
            posInfinity.Checked = false;
            negInfinity.Checked = false;
        }
        unsafe private void operation(int oper) {
            // op: 0 - result, 1 - uminus, 2 - ch_radix, 3 - add, 4 - sub, 5 - mul
            //parse:
            string s=input_num.Text;
            int r=0;
            Double2 t;
            try
            {
                r = (int.Parse(radix.Text));
                t = new Double2(s,current_radix);
            }
            catch (Exception e)
            {
                MessageBox.Show("incorrect number: "+e.Message);
                return;
            }
//            lab_bit.Text = "";
//            lab_bit.Text = "0  12345678   90111111119011111111901";
//            lab_for_float.Text = "";
//            lab_for_float.Text = "s   exponent    fraction";
            if (oper == Oper.ch_radix)
            {
                if (r < 2 || r > 36) {
                    MessageBox.Show("radix must be <=36 and >=2");
                    radix.Text = current_radix.ToString();
                    return;
                }
                t.ch_radix(r);
                current_radix = r;
                input_num.Focus();
                input_num.Text = t.ToString();
                input_num.SelectionStart = 0;
                input_num.SelectionLength = input_num.Text.Length;
                return;
            }else if(oper==Oper.uminus){
                t.uminus();
                input_num.Text = t.ToString();
                input_num.SelectionStart = 0;
                input_num.SelectionLength = input_num.Text.Length;
                return;
            }else if (oper == Oper.int_bit) {
                sp_float_uncheck();
                int n = t.int_part();
                Int2 h = new Int2(n, 2);
                string s1="";
                for (int i = 0; i < sizeof(int); i++)
                {
                    for (int j = 0; j < 8; j++)
                    {
                        if(h.num[Int2.lenmas-32+i*8+j]==1) s1 += "1";
                        else s1 += "0";
                    }
                    s1 += " ";
                }
                lab_bit.Text = s1;
                string sp = " ";
                for (int i = 0; i < 6; i++)sp+="_";
                lab_for_float.Text = sp+"24"+sp+"16"+sp+" 8"+sp+"_0";
                return;
            } else if (oper == Oper.float_bit){
                sp_float_uncheck();
                Double2 w = t.clone();
                t.ch_radix(10);
                string ss = t.ToString();
                int g = ss.IndexOf(",");
                float f;
                if (g != -1) {
                    ss=ss.Substring(0,g) +"."+ ss.Substring(g+1);
                }
                f = float.Parse(ss,System.Globalization.CultureInfo.InvariantCulture);
                string s1 = "";
                s1 = bit_float(f);
                lab_bit.Text = s1;
                lab_for_float.Text = float_prompt;
                return;
/*
                w.ch_radix(2);
                string s1 = "";
                int[] m = w.getFr();
//                if (w.getSign() < 0) mc[0] = '1';
//                else mc[0]= "0 ";
                if (w.getSign() < 0) s1 += "1 ";
                else s1 += "0 ";
                if (w.isZero()){
                    for (int i = 0; i < 8; i++) s1 += "0";
                    s1 += " ";
                    for (int i = 0; i < 23; i++) s1 += "0";
                }else{
                    Int2 g = new Int2(w.getPow()-1 + 127, 2);
                    for (int i = 0; i < 8; i++)
                    {
                        if (g.num[Int2.lenmas - 8 + i] == 1) s1 += "1";
                        else s1 += "0";
                    }
                    s1 += "  ";
                    for (int i = 1; i < 24; i++)
                        if (m[i] == 1) s1 += "1";
                        else s1 += "0";
                }
                lab_bit.Text = s1;
                lab_for_float.Text = "s   exponent    fraction";
                return;
 */
            }
            //state:
            if (state_calc == 0) {
                w1 = t;
                switch (oper)
                {
                    case Oper.add:
                    case Oper.sub: last_oper = oper; state_calc = 1; break;
                    case Oper.mul: /*last_oper = oper;*/ state_calc = 3; break;
                    case Oper.result: break;
                }
            }else if (state_calc == 1) {
                switch (oper){
                    case Oper.add: 
                    case Oper.sub:
                        if (last_oper == Oper.add) w1.add(t);
                        else w1.sub(t);
                        last_oper = oper; state_calc = 1; break;
                    case Oper.mul:
                        w2 = t;
                        lm_oper = last_oper;
                        last_oper = oper; state_calc = 2; break;
                    case Oper.result:
                        if (last_oper == Oper.add) w1.add(t);
                        else w1.sub(t);
                        state_calc = 0;
                        break;
                }
                if (state_calc == 2) t = w2;
                else t = w1;
            }
            else if (state_calc == 2) {//last_oper == mul, befor it lm_oper
                switch (oper)
                {
                    case Oper.add:
                    case Oper.sub:
                        w2.mul(t);
                        if (lm_oper == Oper.add) w1.add(w2);
                        else w1.sub(w2);
                        last_oper = oper; state_calc = 1; break;
                    case Oper.mul:
                        w2.mul(t);
                        state_calc = 2; break;
                    case Oper.result:
                        w2.mul(t);
                        if (lm_oper == Oper.add) w1.add(w2);
                        else w1.sub(w2);
                        state_calc = 0;
                        break;
                }
                if (state_calc == 0 || state_calc==1) t = w1;
                else t = w2;
            }
            else if (state_calc == 3){//last_oper == mul
                switch (oper)
                {
                    case Oper.add:
                    case Oper.sub:
                        w2 = t;
                        w1.mul(w2);
                        last_oper = oper; state_calc = 1; break;
                    case Oper.mul:
                        w1.mul(t);
                        state_calc = 3; break;
                    case Oper.result:
                        w1.mul(t);
                        state_calc = 0;
                        break;
                }
//                if (state_calc == 0) t = w1;
//                else
                t = w1;
            }
            current_radix = t.getRadix();
            radix.Text = current_radix.ToString();
            input_num.Text = t.ToString();
            if (t.getPow() > Double2.lenmas)
                MessageBox.Show("the number out of range. only left "+Double2.lenmas.ToString()+" digits correct");
            input_num.SelectionStart = 0;
            input_num.SelectionLength = input_num.Text.Length;
        }

        private void add_Click(object sender, EventArgs e)
        {
            operation(Oper.add);
        }

        private void sub_Click(object sender, EventArgs e)
        {
            operation(Oper.sub);
        }

        private void mul_Click(object sender, EventArgs e)
        {
            operation(Oper.mul);
        }

        private void eq_Click(object sender, EventArgs e)
        {
            operation(Oper.result);
        }

        private void input_num_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '+'){
                operation(Oper.add);
            }else if (e.KeyChar == '-'){
                operation(Oper.sub);
            }else if (e.KeyChar == '*'){
                operation(Oper.mul);
            }else if (e.KeyChar == 13){
                operation(Oper.result);
            }
            if (!(is_rdigit(e.KeyChar)) && 
                !(Double2.getCalcMode()==CalcMode.real && (e.KeyChar == '.' || e.KeyChar == ',') && 
                    (input_num.Text.IndexOf(".") == -1 && input_num.Text.IndexOf(",") == -1)))
            {
                if (e.KeyChar != (char)Keys.Back)
                {
                    e.Handled = true;
                }
            }
        }

        private void radix_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != (char)Keys.Back)
            {
                if (e.KeyChar == 13) {
                    operation(Oper.ch_radix); 
                }
                if (!(Char.IsDigit(e.KeyChar)) || (radix.Text.Length >= 2 && radix.SelectionLength == 0)){
                    e.Handled = true;
                }
            }
        }

        private void radix_Leave(object sender, EventArgs e)
        {
            radix.Text = current_radix.ToString();
        }

        private void clear_Click(object sender, EventArgs e)
        {
            state_calc = 0;
            input_num.Text = "0";
            current_radix = 10;
            radix.Text = current_radix.ToString();
            input_num.Focus();
            input_num.SelectionStart = 0;
            input_num.SelectionLength = input_num.Text.Length;
        }

        private void int_bit_Click(object sender, EventArgs e)
        {
            operation(Oper.int_bit);
        }

        private void float_bit_Click(object sender, EventArgs e)
        {
            operation(Oper.float_bit);
        }

        private void sp_float(float f){
            lab_bit.Text = bit_float(f);
            lab_for_float.Text = float_prompt;
        }

        private void posInfinity_CheckedChanged(object sender, EventArgs e)
        {
            sp_float(1.0f/0.0f);
        }

        private void negInfinity_CheckedChanged(object sender, EventArgs e)
        {
            sp_float(-1.0f/0.0f);
        }

        private void NAN_CheckedChanged(object sender, EventArgs e)
        {
            sp_float(0.0f/0.0f);
        }

        private void firstToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string s,h="help";
            s = "calc mode:\n";
            s += "\t real\t - \tsimple calc\n";
            s += "\t int16\t - \tint calc in 16-bit number\n";
            s += "\t int32\t - \tint calc in 32-bit number\n";
            s += "special float - format of special values of float:\n";
            s += "\t NaN\t - \tnot a number\n";
            s += "\t +infinity\t - \tpositive infinity\n";
            s += "\t -infinity\t - \tnegative infinity\n";
            s += "bit buttons:\n";
            s += "\t int bit\t - \tbinary int32\n";
            s += "\t float bit\t - \tbinary float IEEE-754\n";
            s += "radix - field for base of system of number.\n";
            s += "\t (press enter after change it)\n";
            MessageBox.Show(s, h);
        }

        private void cm_real_CheckedChanged(object sender, EventArgs e)
        {
            Double2.setCalcMode(CalcMode.real);
        }

        private void cm_int32_CheckedChanged(object sender, EventArgs e)
        {
            Double2.setCalcMode(CalcMode.int32);
            string s = input_num.Text;
            int di = Math.Max(s.IndexOf('.'), s.IndexOf(','));
            if(di>=0)input_num.Text = s.Substring(0,di);
        }

        private void cm_int16_CheckedChanged(object sender, EventArgs e)
        {
            Double2.setCalcMode(CalcMode.int16);
            string s = input_num.Text;
            int di = Math.Max(s.IndexOf('.'), s.IndexOf(','));
            if (di >= 0) input_num.Text = s.Substring(0, di);
        }

        private void menu_About_Click(object sender, EventArgs e)
        {
            MessageBox.Show("© copyrights by Prigornev Alexander 2011","About");
        }

    }
}
