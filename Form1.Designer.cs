﻿namespace курсовая_2011
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.input_num = new System.Windows.Forms.TextBox();
            this.radix = new System.Windows.Forms.TextBox();
            this.add = new System.Windows.Forms.Button();
            this.mul = new System.Windows.Forms.Button();
            this.sub = new System.Windows.Forms.Button();
            this.eq = new System.Windows.Forms.Button();
            this.uminus = new System.Windows.Forms.Button();
            this.clear = new System.Windows.Forms.Button();
            this.float_bit = new System.Windows.Forms.Button();
            this.int_bit = new System.Windows.Forms.Button();
            this.lab_bit = new System.Windows.Forms.Label();
            this.lab_for_float = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.negInfinity = new System.Windows.Forms.RadioButton();
            this.posInfinity = new System.Windows.Forms.RadioButton();
            this.NAN = new System.Windows.Forms.RadioButton();
            this.group_calc_mode = new System.Windows.Forms.GroupBox();
            this.cm_int16 = new System.Windows.Forms.RadioButton();
            this.cm_int32 = new System.Windows.Forms.RadioButton();
            this.cm_real = new System.Windows.Forms.RadioButton();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menu = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Help = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_About = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1.SuspendLayout();
            this.group_calc_mode.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // input_num
            // 
            this.input_num.Location = new System.Drawing.Point(28, 36);
            this.input_num.Name = "input_num";
            this.input_num.Size = new System.Drawing.Size(301, 20);
            this.input_num.TabIndex = 0;
            this.input_num.Text = "0";
            this.input_num.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.input_num.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.input_num_KeyPress);
            // 
            // radix
            // 
            this.radix.Location = new System.Drawing.Point(247, 62);
            this.radix.Name = "radix";
            this.radix.Size = new System.Drawing.Size(82, 20);
            this.radix.TabIndex = 1;
            this.radix.Text = "10";
            this.radix.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.radix.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.radix_KeyPress);
            this.radix.Leave += new System.EventHandler(this.radix_Leave);
            // 
            // add
            // 
            this.add.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.add.Location = new System.Drawing.Point(28, 62);
            this.add.Name = "add";
            this.add.Size = new System.Drawing.Size(30, 30);
            this.add.TabIndex = 3;
            this.add.Text = " +";
            this.add.UseVisualStyleBackColor = true;
            this.add.Click += new System.EventHandler(this.add_Click);
            // 
            // mul
            // 
            this.mul.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.mul.Location = new System.Drawing.Point(100, 62);
            this.mul.Name = "mul";
            this.mul.Size = new System.Drawing.Size(30, 30);
            this.mul.TabIndex = 4;
            this.mul.Text = " *";
            this.mul.UseVisualStyleBackColor = true;
            this.mul.Click += new System.EventHandler(this.mul_Click);
            // 
            // sub
            // 
            this.sub.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sub.Location = new System.Drawing.Point(64, 62);
            this.sub.Name = "sub";
            this.sub.Size = new System.Drawing.Size(30, 30);
            this.sub.TabIndex = 5;
            this.sub.Text = " -";
            this.sub.UseVisualStyleBackColor = true;
            this.sub.Click += new System.EventHandler(this.sub_Click);
            // 
            // eq
            // 
            this.eq.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.eq.Location = new System.Drawing.Point(172, 62);
            this.eq.Name = "eq";
            this.eq.Size = new System.Drawing.Size(30, 30);
            this.eq.TabIndex = 6;
            this.eq.Text = " =";
            this.eq.UseVisualStyleBackColor = true;
            this.eq.Click += new System.EventHandler(this.eq_Click);
            // 
            // uminus
            // 
            this.uminus.Location = new System.Drawing.Point(136, 62);
            this.uminus.Name = "uminus";
            this.uminus.Size = new System.Drawing.Size(30, 30);
            this.uminus.TabIndex = 9;
            this.uminus.Text = "+/-";
            this.uminus.UseVisualStyleBackColor = true;
            this.uminus.Click += new System.EventHandler(this.uminus_Click);
            // 
            // clear
            // 
            this.clear.Location = new System.Drawing.Point(348, 36);
            this.clear.Name = "clear";
            this.clear.Size = new System.Drawing.Size(71, 32);
            this.clear.TabIndex = 11;
            this.clear.Text = "Clear";
            this.clear.UseVisualStyleBackColor = true;
            this.clear.Click += new System.EventHandler(this.clear_Click);
            // 
            // float_bit
            // 
            this.float_bit.Location = new System.Drawing.Point(254, 88);
            this.float_bit.Name = "float_bit";
            this.float_bit.Size = new System.Drawing.Size(75, 23);
            this.float_bit.TabIndex = 12;
            this.float_bit.Text = "float bit";
            this.float_bit.UseVisualStyleBackColor = true;
            this.float_bit.Click += new System.EventHandler(this.float_bit_Click);
            // 
            // int_bit
            // 
            this.int_bit.Location = new System.Drawing.Point(254, 117);
            this.int_bit.Name = "int_bit";
            this.int_bit.Size = new System.Drawing.Size(75, 23);
            this.int_bit.TabIndex = 13;
            this.int_bit.Text = "int bit";
            this.int_bit.UseVisualStyleBackColor = true;
            this.int_bit.Click += new System.EventHandler(this.int_bit_Click);
            // 
            // lab_bit
            // 
            this.lab_bit.AutoSize = true;
            this.lab_bit.Location = new System.Drawing.Point(25, 106);
            this.lab_bit.Name = "lab_bit";
            this.lab_bit.Size = new System.Drawing.Size(214, 13);
            this.lab_bit.TabIndex = 14;
            this.lab_bit.Text = "0  00000000   00000000000000000000000";
            // 
            // lab_for_float
            // 
            this.lab_for_float.AutoSize = true;
            this.lab_for_float.Location = new System.Drawing.Point(25, 130);
            this.lab_for_float.Name = "lab_for_float";
            this.lab_for_float.Size = new System.Drawing.Size(112, 13);
            this.lab_for_float.TabIndex = 15;
            this.lab_for_float.Text = "s   exponent    fraction";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(209, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "radix:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.negInfinity);
            this.groupBox1.Controls.Add(this.posInfinity);
            this.groupBox1.Controls.Add(this.NAN);
            this.groupBox1.Location = new System.Drawing.Point(28, 156);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(181, 37);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "special float";
            // 
            // negInfinity
            // 
            this.negInfinity.AutoSize = true;
            this.negInfinity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.negInfinity.Location = new System.Drawing.Point(115, 14);
            this.negInfinity.Name = "negInfinity";
            this.negInfinity.Size = new System.Drawing.Size(62, 17);
            this.negInfinity.TabIndex = 2;
            this.negInfinity.Text = "– infinity";
            this.negInfinity.UseVisualStyleBackColor = true;
            this.negInfinity.CheckedChanged += new System.EventHandler(this.negInfinity_CheckedChanged);
            // 
            // posInfinity
            // 
            this.posInfinity.AutoSize = true;
            this.posInfinity.Location = new System.Drawing.Point(52, 14);
            this.posInfinity.Name = "posInfinity";
            this.posInfinity.Size = new System.Drawing.Size(63, 17);
            this.posInfinity.TabIndex = 1;
            this.posInfinity.Text = "+ infinity";
            this.posInfinity.UseVisualStyleBackColor = true;
            this.posInfinity.CheckedChanged += new System.EventHandler(this.posInfinity_CheckedChanged);
            // 
            // NAN
            // 
            this.NAN.AutoSize = true;
            this.NAN.Location = new System.Drawing.Point(6, 14);
            this.NAN.Name = "NAN";
            this.NAN.Size = new System.Drawing.Size(47, 17);
            this.NAN.TabIndex = 0;
            this.NAN.Text = "NaN";
            this.NAN.UseVisualStyleBackColor = true;
            this.NAN.CheckedChanged += new System.EventHandler(this.NAN_CheckedChanged);
            // 
            // group_calc_mode
            // 
            this.group_calc_mode.Controls.Add(this.cm_int16);
            this.group_calc_mode.Controls.Add(this.cm_int32);
            this.group_calc_mode.Controls.Add(this.cm_real);
            this.group_calc_mode.Location = new System.Drawing.Point(468, 36);
            this.group_calc_mode.Name = "group_calc_mode";
            this.group_calc_mode.Size = new System.Drawing.Size(71, 83);
            this.group_calc_mode.TabIndex = 22;
            this.group_calc_mode.TabStop = false;
            this.group_calc_mode.Text = "calc mode";
            // 
            // cm_int16
            // 
            this.cm_int16.AutoSize = true;
            this.cm_int16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cm_int16.Location = new System.Drawing.Point(6, 61);
            this.cm_int16.Name = "cm_int16";
            this.cm_int16.Size = new System.Drawing.Size(48, 17);
            this.cm_int16.TabIndex = 2;
            this.cm_int16.Text = "int16";
            this.cm_int16.UseVisualStyleBackColor = true;
            this.cm_int16.CheckedChanged += new System.EventHandler(this.cm_int16_CheckedChanged);
            // 
            // cm_int32
            // 
            this.cm_int32.AutoSize = true;
            this.cm_int32.Location = new System.Drawing.Point(6, 38);
            this.cm_int32.Name = "cm_int32";
            this.cm_int32.Size = new System.Drawing.Size(48, 17);
            this.cm_int32.TabIndex = 1;
            this.cm_int32.Text = "int32";
            this.cm_int32.UseVisualStyleBackColor = true;
            this.cm_int32.CheckedChanged += new System.EventHandler(this.cm_int32_CheckedChanged);
            // 
            // cm_real
            // 
            this.cm_real.AutoSize = true;
            this.cm_real.Checked = true;
            this.cm_real.Location = new System.Drawing.Point(6, 15);
            this.cm_real.Name = "cm_real";
            this.cm_real.Size = new System.Drawing.Size(42, 17);
            this.cm_real.TabIndex = 0;
            this.cm_real.TabStop = true;
            this.cm_real.Text = "real";
            this.cm_real.UseVisualStyleBackColor = true;
            this.cm_real.CheckedChanged += new System.EventHandler(this.cm_real_CheckedChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(551, 24);
            this.menuStrip1.TabIndex = 23;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menu
            // 
            this.menu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Help,
            this.menu_About});
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(45, 20);
            this.menu.Text = "menu";
            // 
            // menu_Help
            // 
            this.menu_Help.Name = "menu_Help";
            this.menu_Help.Size = new System.Drawing.Size(152, 22);
            this.menu_Help.Text = "Help";
            this.menu_Help.Click += new System.EventHandler(this.firstToolStripMenuItem_Click);
            // 
            // menu_About
            // 
            this.menu_About.Name = "menu_About";
            this.menu_About.Size = new System.Drawing.Size(152, 22);
            this.menu_About.Text = "About";
            this.menu_About.Click += new System.EventHandler(this.menu_About_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(551, 347);
            this.Controls.Add(this.group_calc_mode);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lab_for_float);
            this.Controls.Add(this.lab_bit);
            this.Controls.Add(this.int_bit);
            this.Controls.Add(this.float_bit);
            this.Controls.Add(this.clear);
            this.Controls.Add(this.uminus);
            this.Controls.Add(this.eq);
            this.Controls.Add(this.sub);
            this.Controls.Add(this.mul);
            this.Controls.Add(this.add);
            this.Controls.Add(this.radix);
            this.Controls.Add(this.input_num);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "calc";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.group_calc_mode.ResumeLayout(false);
            this.group_calc_mode.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox input_num;
        private System.Windows.Forms.TextBox radix;
        private System.Windows.Forms.Button add;
        private System.Windows.Forms.Button mul;
        private System.Windows.Forms.Button sub;
        private System.Windows.Forms.Button eq;
        private System.Windows.Forms.Button uminus;
        private System.Windows.Forms.Button clear;
        private System.Windows.Forms.Button float_bit;
        private System.Windows.Forms.Button int_bit;
        private System.Windows.Forms.Label lab_bit;
        private System.Windows.Forms.Label lab_for_float;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton NAN;
        private System.Windows.Forms.RadioButton negInfinity;
        private System.Windows.Forms.RadioButton posInfinity;
        private System.Windows.Forms.GroupBox group_calc_mode;
        private System.Windows.Forms.RadioButton cm_int16;
        private System.Windows.Forms.RadioButton cm_int32;
        private System.Windows.Forms.RadioButton cm_real;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menu;
        private System.Windows.Forms.ToolStripMenuItem menu_About;
        private System.Windows.Forms.ToolStripMenuItem menu_Help;
    }
}

